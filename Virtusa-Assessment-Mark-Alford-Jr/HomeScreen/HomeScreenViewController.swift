//
//  ViewController.swift
//  Virtusa-Assessment-Mark-Alford-Jr
//
//  Created by Mark Alford on 3/15/23.
//

import UIKit

class HomeScreenViewController: UIViewController {

    let viewModel = HomeScreenViewModel()
    
    //MARK: - Tableview Setup
    
    // the tableview array, along with the tableview setup
    var dataArray: SchoolModelArray = []
    let tableview: UITableView = {
        let table = UITableView()
        table.register(SchoolTableViewCell.self, forCellReuseIdentifier: SchoolTableViewCell.identifier)
        return table
    }()

    //MARK: - Initilaizer
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        view.addSubview(tableview)
        tableview.delegate = self
        tableview.dataSource = self
        
        viewModel.updateUI = { [weak self] newdata in
            DispatchQueue.main.async {
                self?.dataArray = newdata ?? []
                self?.tableview.reloadData()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.fetchRequestData()
    }
    
    override func viewDidLayoutSubviews() {
        tableview.frame = view.bounds
    }
}

//MARK: - TableView Configuration
extension HomeScreenViewController: UITableViewDelegate, UITableViewDataSource {
    /// determines the number of rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    /// configure EACH tableview row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableview.dequeueReusableCell(withIdentifier: SchoolTableViewCell.identifier, for: indexPath) as? SchoolTableViewCell else {
            fatalError("TableviewCell not working")
        }
        let cellArray = dataArray[indexPath.row]
        cell.configureCell(dbn: cellArray.dbn, school: cellArray.schoolName, phone: cellArray.phoneNumber, neighborhood: cellArray.neighborhood, address: cellArray.location)
        return cell
    }
    
    /// sets the height for the row
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    //MARK: - Navigation
    /// allows Navigation to the SATscore VC, populating that screen with EACH cell's unique JSON data
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cellArray = dataArray[indexPath.row]
        let scoreVc = SatScoresScreenViewController(dbnName: cellArray.dbn, school: cellArray.schoolName)
        navigationController?.pushViewController(scoreVc, animated: true)
    }
}
