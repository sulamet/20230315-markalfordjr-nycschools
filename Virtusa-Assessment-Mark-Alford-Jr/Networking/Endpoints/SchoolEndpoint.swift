//
//  SchoolEndpoint.swift
//  Virtusa-Assessment-Mark-Alford-Jr
//
//  Created by Mark Alford on 3/15/23.
//

import Foundation

// https://data.cityofnewyork.us/resource/s3k6-pzi2.json
/// The endpoint used for providing a customized URLComponent for the NetworkManager class
enum SchoolAPI: API {
    case getschools
    
    var scheme: HTTPScheme {
        switch self {
        case .getschools:
            return .https
        }
    }
    
    var baseURL: String {
        switch self {
        case .getschools:
            return "data.cityofnewyork.us"
        }
    }
    
    var path: String {
        switch self {
        case .getschools:
            return "/resource/s3k6-pzi2.json"
        }
    }
    
    var parameters: [URLQueryItem] {
        switch self {
        case .getschools:
            return []
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .getschools:
            return .get
        }
    }
}
