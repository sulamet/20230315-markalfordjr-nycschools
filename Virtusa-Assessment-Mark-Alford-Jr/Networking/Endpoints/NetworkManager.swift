//
//  NetworkManager.swift
//  Virtusa-Assessment-Mark-Alford-Jr
//
//  Created by Mark Alford on 3/15/23.
//

import Foundation

final class NetworkManager {
    /**
     Builds the Relative URL Components from the values specified in
     the API
     - Parameters endpoint: the endpoint enum to make the HTTP request to
     - Returns: the URLComponent needed to build the url fro the URLRequest inside the request func
     */
    private class func buildURL(endpoint: API) -> URLComponents {
        var components = URLComponents()
        components.scheme = endpoint.scheme.rawValue
        components.host = endpoint.baseURL
        components.path = endpoint.path
        components.queryItems = endpoint.parameters
        return components
    }
    
    /**
     Executes the HTTP request and will attempt to decode the JSON
     response into a Codable object.
     - Parameters:
        - endpoint: the endpoint to make the HTTP request to
        - completion: the closure for the decoded JSON to be handled in, when the api is called, returns void
     */
    class func request<T: Decodable>(endpoint: API, completion: @escaping (Result<T, Error>) -> Void) {
        
        let components = buildURL(endpoint: endpoint)
        guard let url = components.url else {
            print("URL creation error")
            return
        }
        // create the request
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = endpoint.method.rawValue
        // create the request session
        let session = URLSession(configuration: .default)
        
        /// where the JSON decoding process takes place
        let dataTask = session.dataTask(with: urlRequest) { data, response, error in
            if let error = error {
                completion(.failure(error))
                print("Unknown error", error)
                return
            }
            
            guard response != nil, let data = data else {
                return
            }
            // use a generic class, to allow different API enums to be decoded
            if let responseObject = try? JSONDecoder().decode(T.self, from: data) {
                completion(.success(responseObject))
            } else {
                let error = NSError(domain: "",
                                    code: 200,
                                    userInfo: [
                                        NSLocalizedDescriptionKey: "Failed"
                                    ])
                
                completion(.failure(error))
            }
        }
        dataTask.resume()
    }
}
