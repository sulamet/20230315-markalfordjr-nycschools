//
//  SchoolModel.swift
//  Virtusa-Assessment-Mark-Alford-Jr
//
//  Created by Mark Alford on 3/15/23.
//

import Foundation

/// Codable Data Model Struct for the School API's JSON
struct SchoolModel: Codable {
    let dbn, schoolName, location, phoneNumber, neighborhood: String
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case location = "location"
        case phoneNumber = "phone_number"
        case neighborhood = "neighborhood"
    }
}

typealias SchoolModelArray = [SchoolModel]
