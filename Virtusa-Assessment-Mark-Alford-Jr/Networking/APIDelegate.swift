//
//  APIDelegate.swift
//  Virtusa-Assessment-Mark-Alford-Jr
//
//  Created by Mark Alford on 3/15/23.
//

import Foundation


/// provide the HTTP methods, in order to customize any API
enum HTTPMethod: String {
    case delete = "DELETE"
    case get = "GET"
    case patch = "PATCH"
    case post = "POST"
    case put = "PUT"
}

/// provide the HTTP schemes, for the url being secure or not
enum HTTPScheme: String {
    case http
    case https
}

/// Provides template for all APIs to inherit from, thus allowing customization for their URLs
protocol API {
    // http or https
    var scheme: HTTPScheme { get }
    // Example: "maps.googleapis.com"
    var baseURL: String { get }
    // "/maps/api/place/nearbysearch/"
    var path: String { get }
    // [URLQueryItem(name: "api_key", value: API_KEY)]
    var parameters: [URLQueryItem] { get }
    // "GET"
    var method: HTTPMethod { get }
}
